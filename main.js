class Visit {

    constructor(options) { // Получаем ДОМ элементы и вызываем метод накидывания события

        this.date = new Date();
        this.modal = document.querySelector('.modalBody');
        this.cardContainer = document.querySelector('.visits');
        this.select = document.querySelector('select');
        this.selectData();
        this.dropContainer();

    }


    dropContainer() {
        const dragEnter = function (evt) {
            evt.preventDefault();
            this.classList.add('hovered');
            return true;
        };

        const dragLeave = function (evt) {
            this.classList.remove('hovered');
        };

        const dragOver = function (evt) {
            evt.preventDefault();
        };

        const dragDrop = function (evt) {
            this.classList.remove('hovered');

            evt.stopPropagation();
            return false;
        };


        this.cardContainer.addEventListener('dragenter', dragEnter);
        this.cardContainer.addEventListener('dragover', dragOver);
        this.cardContainer.addEventListener('dragleave', dragLeave);
        this.cardContainer.addEventListener('drop', dragDrop);

    };

    selectData() { // Проверяем значение селекта, и создаем нужный инстанс
        this.select.addEventListener('change', function (e) {
            if (select.value === 'dentist') {
                const visit = new Dentist();
                visit.renderField(); // Создаем поля для нужного инстанса

            } else if (select.value === 'cardiologist') {
                const visit = new Cardiologist();
                visit.renderField();

            } else if (select.value === 'therapist') {
                const visit = new Therapist();
                visit.renderField();

            } else {
                let prevResults = document.querySelectorAll('.result');
                prevResults.forEach(function (i) {
                    i.remove();
                }) // Если селект пустой, делаем очистку модалки
            }
        });
    }


    renderField() { // Метод создания полей в модалке
        let result = document.createElement('div');
        result.className = 'result';
        let field = this.field;
        let prevResults = document.querySelectorAll('.result');
        prevResults.forEach(function (i) {
            i.remove();
        });

        this.field.map(function (i) {   // Бегаем по массиву нужных нам полей (static fields), и выводим каждый элемент с инпутом
            result.innerHTML += `<li>${i}</li>
                                 <input>`;
        });

        result.innerHTML += `<li>Комментарий</li>
                            <input value="" class="comment">`;
        result.innerHTML += `<button name="created" value="Created" class="created" id="created"><a href="#">Created</a></button>`; // Создаем кнопку в модалке
        this.modal.append(result);

        let commentArea = document.querySelector('.comment');

        commentArea.addEventListener('input', function () {
            if (commentArea.value.length > 400) {
                commentArea.value = commentArea.value.slice(0, -1);
            }
        })

        let buttonCreated = document.querySelector('.created');
        let buttonClose = document.querySelector('.close');
        let arr = [];

        let binded = this.renderCard.bind(this); // Создаем бинд метода, что бы использовать его в функции через this
        buttonCreated.addEventListener('click', function () { // Событие кнопки создания карточки

            let inputs = document.querySelectorAll('input');
            inputs.forEach(function (i) {
                console.log(i.value.length);
                if (i.value.length > 0) {
                    arr.push(i.value);
                }// Пушим в массив значения инпутов из модалки
            })


            if (arr.length >= field.length) {
                binded(arr, select);
                let prevResults = document.querySelectorAll('.result');
                prevResults.forEach(function (i) {
                    i.remove();
                })
                select.value = null;
            } else {
                result.innerHTML += "<li>ВЫ НЕ ЗАПОЛНИЛИ ВСЕ ПОЛЯ!</li>";
            }// Вызов метода renderCard() с передачей массива с занчениями инпутов в модалке


        })
        buttonClose.addEventListener('click', function () { // Событие кнопки закрытия модалки
            select.value = null; // Обнуляем значение селекта
            let prevResults = document.querySelectorAll('.result');
            prevResults.forEach(function (i) {
                i.remove();
            })
        })
    }

    renderCard(arr, select) { // Метода создание карточки
        let visit = this.cardContainer;
        let container = document.createElement('div');
        container.className = 'containerForCard';

        let ul = document.createElement('ul'); // Создаем контейнер для карточки

        let ulHide = document.createElement('ul');
        ulHide.className = 'hide';
        let field = this.field;
        container.setAttribute('draggable', 'true');

        ul.innerHTML += `<a href="#" class="deleteCard">X</a>`;

        ul.innerHTML += `<li>${select.value.toUpperCase()}</li>`;

        if (arr.length > field.length) {
            field.push('Комментарий');
        }

        ul.innerHTML += `<li>${field[0]}: ${arr[0]}</li>`;

        field.forEach(function (item, i) { // Бегаем по массиву с фиелдами и массиву с значениями инпутов, выводим относительно своих индексов
            if (i > 0) {
                ulHide.innerHTML += `<li>${item}: ${arr[i]}</li>`;
            }
        })
        ul.innerHTML += `<a href="#" class="showMore">Показать больше...</a>`;


        container.append(ul);
        container.append(ulHide);
        visit.append(container);


        let showMore = container.querySelector('.showMore');

        showMore.addEventListener('click', function (e) {
            e.preventDefault();
            console.log(12321);
            ulHide.className = 'show';
            showMore.remove();
        });


        let deleteCardBtn = ul.querySelector('.deleteCard');

        deleteCardBtn.addEventListener('click', function (e) {
            e.preventDefault();
            container.remove();
        })


        const dragStart = function (evt) {
            this.classList.add('draggable');
            setTimeout(() => {
                this.classList.add('hide');
            }, 0)
        };

        const dragEnd = function (evt) {
            this.classList.remove('hide');
            this.classList.remove('draggable');
            this.style.position = 'absolute';
            console.log(this.offsetWidth);
            this.style.left = evt.pageX - (this.offsetWidth / 2) + 'px';
            this.style.top = (evt.pageY - this.offsetHeight) / 2 + 'px';
        };


        container.addEventListener('dragstart', dragStart);
        container.addEventListener('dragend', dragEnd);


    }

    static fields = {
        cardiologist: ["ФИО", "Цель визита", "Обычное давление", "Индекс массы тела", "Перенесенные заболевания сердечно-сосудистой системы", "Возраст"],
        dentist: ["ФИО", "Цель визита", "Дата последнего посещения"],
        therapist: ["ФИО", "Цель визита", "Возраст"],
    }
}

class Cardiologist extends Visit {
    constructor(options) {
        super(options);
        this.field = Visit.fields.cardiologist;
    }


}

class Dentist extends Visit {
    constructor(options) {
        super(options);
        this.field = Visit.fields.dentist;
    }
}

class Therapist extends Visit {
    constructor(options) {
        super(options);
        this.field = Visit.fields.therapist;
    }
}

////////////////////////////////////////////

class Db { // класс, который предоставляет методы для взаимодействия с сервером

    constructor() {
        this._uri = 'http://cards.danit.com.ua/login';
        // this._email = "111vadim111@ukr.net"
        // this._password = "1q2w3e4r"
        this._email = "123@gmail.com";
        this._password = "123"
    }

    token = '';


    getToken() { //получение токена. Возвращает промис: токен или ошибку
        // Полученный токен сохраняется в this.token.
        //Если this.token не пустой, обращение к серверу не происходит
        //метод возвращает знаечение this.token

        return new Promise((resolve, reject) => {

                if (this.token !== '') {
                    resolve(this.token)
                }

                let request = new XMLHttpRequest();
                request.open('POST', this._uri);

                let data = {
                    email: this._email,
                    password: this._password
                };

                data = JSON.stringify(data);
                request.responseType = 'json';
                request.send(data);

                request.onload = (result) => {
                    if (result.target.status == 200) {
                        this.token = result.target.response.token;
                        resolve(this.token)
                    } else {
                        console.error(result);
                        reject(result.target.status)
                    }
                };
            }
        )
    }

    ////////////////////////////////////////
    createCard(data) { //Создание карточки на сервере. Возвращает промис: id созданной карточки или ошибку
        // в data ожидается объект для сохранения

        return new Promise((resolve, reject) => {

            this.getToken()
                .then(
                    (token) => {

                        let request = new XMLHttpRequest();
                        request.open('POST', `http://cards.danit.com.ua/cards`);
                        request.setRequestHeader('Authorization', `Bearer ${token}`)

                        // let data = {
                        //     doctor: "cardiolog",
                        //     title: "Цель визита",
                        //     description: 'Краткое описание визита',
                        //     status: "open",
                        //     priority: "Приоритет",
                        //     content: {

                        //         bp: "24",
                        //         age: 23,
                        //         weight: 70

                        //     }
                        // };

                        data = JSON.stringify(data);
                        request.responseType = 'json';
                        request.send(data);

                        request.onload = (result) => {

                            if (result.target.status == 200) {
                                resolve(result.target.response.id)
                            } else {
                                reject(result.target.status)
                            }

                        };
                    }
                )
                .catch(
                    (err) => console.error(err)
                )
        })
    }

    ////////////////////////////////////////

    updateCard(cardId, data) { // Изменение карточки. Возвращает промис: id измененной карточки или ошибку
        //cardId - ід карточки которая будет изменеа, data - объект для сохранения соответственно спецификации сервера

        return new Promise((resolve, reject) => {

            this.getToken()
                .then(
                    (token) => {

                        let request = new XMLHttpRequest();
                        request.open('put', `http://cards.danit.com.ua/cards/${cardId}`);
                        request.setRequestHeader('Authorization', `Bearer ${token}`);

                        // let data = {
                        //     doctor: "cardiolog",
                        //     title: "Цель визита1",
                        //     description: 'Краткое описание визита',
                        //     status: "open",
                        //     priority: "Приоритет",
                        //     content: {

                        //         bp: "24",
                        //         age: 23,
                        //         weight: 70

                        //     }
                        // };

                        data = JSON.stringify(data);
                        request.responseType = 'json';
                        request.send(data);

                        request.onload = (result) => {

                            if (result.target.status == 200) {
                                resolve(result.target.response.id)
                            } else {
                                reject(result.target.status)
                            }

                        };
                    }
                )
                .catch(
                    (err) => console.error(err)
                )


        })


    }

    ////////////////////////////////////////

    deleteCard(cardID) { //удаление карточки. Возвращает промис: "Success" или ошибку
        //cardID - id карточки

        return new Promise((resolve, reject) => {

            this.getToken()
                .then(
                    (token) => {

                        let request = new XMLHttpRequest();
                        request.open('delete', `http://cards.danit.com.ua/cards/${cardID}`);
                        request.setRequestHeader('Authorization', `Bearer ${token}`);

                        request.responseType = 'json';
                        request.send();

                        request.onload = (result) => {

                            if (result.target.status == 200) {
                                resolve(result.target.response.status)
                            } else {
                                reject(result.target.status)
                            }

                        };
                    }
                )
                .catch(
                    (err) => console.error(err)
                )
        })

    }

    getAllCards() { //Получение списка всех карточек в виде JSON-массива объектов
        // Возвращает промис: массив объектов или ошибку

        return new Promise((resolve, reject) => {

            this.getToken()
                .then(
                    (token) => {

                        let request = new XMLHttpRequest();
                        request.open('get', `http://cards.danit.com.ua/cards`);
                        request.setRequestHeader('Authorization', `Bearer ${token}`);

                        request.responseType = 'json';
                        request.send();

                        request.onload = (result) => {

                            if (result.target.status == 200) {
                                resolve(result.target.response)
                            } else {
                                reject(result.target.status)
                            }

                        };
                    }
                )
                .catch(
                    (err) => console.error(err)
                )
        })

    }

    getCard(cardId) { //Получение карточки по id
        //cardId - id карточки
        //Возвращает промис: объект карточки или ошибку

        return new Promise((resolve, reject) => {

            this.getToken()
                .then(
                    (token) => {

                        let request = new XMLHttpRequest();
                        request.open('get', `http://cards.danit.com.ua/cards/${cardId}`);
                        request.setRequestHeader('Authorization', `Bearer ${token}`)

                        request.responseType = 'json';
                        request.send();

                        request.onload = (result) => {

                            if (result.target.status == 200) {
                                resolve(result.target.response)
                            } else {
                                reject(result.target.status)
                            }

                        };
                    }
                )
                .catch(
                    (err) => console.error(err)
                )
        })

    }

}

//////////////////////////////////////


let visit = new Visit();

////////////////////////////////////////////////////////////////////////
//тест Db

const db = new Db();
let cardId = '';
// Получение токена.
db.getToken().then((token) => console.log('token', token)).catch((err) => console.error(err));

const cardData1 = {
    doctor: "cardiolog",
    title: "Цель визита № 1",
    description: 'Краткое описание визита № 1',
    status: "open",
    priority: "Приоритет",
    content: {

        bp: "24",
        age: 23,
        weight: 70

    }
};

const cardData2 = {
    doctor: "cardiolog",
    title: "Цель визита № 2",
    description: 'Краткое описание визита № 2',
    status: "open",
    priority: "Приоритет",
    content: {

        bp: "24",
        age: 23,
        weight: 70

    }
};

db.createCard(cardData1) //создание карточки
    .then((id) => {
            console.log('card creted with id: ', id);
            cardId = id; //id новосозданной карточки

            // Получение всех карточек.
            db.getAllCards()
                .then((list) => console.log('getting all cards list : ', list))
                .catch((err) => console.error('getting all cards list error: ', err));
            //

            db.getCard(cardId) //получение карточки по id
                .then((card) => {

                    console.log('getting card by id ', card);

                    db.updateCard(cardId, cardData2) // замена карточки
                        .then((card) => {

                            console.log('card udated : ', card);

                            //удаление карточки
                            db.deleteCard(cardId)
                                .then((status) => console.log('card deleted: ', status))
                                .catch((err) => console.error('card deleted error: ', err))

                        }).catch((err) => console.error('card udated error: ', err))

                })
                .catch((err) => console.error('getting card by id error: ', err))


        }
    )
    .catch((err) => console.error('card created with id error: ', err));

    